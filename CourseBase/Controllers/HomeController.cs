﻿using CourseBase.Entities;
using CourseBase.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CourseBase.Controllers
{
    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /* public ActionResult Personal()
         {
             ViewBag.Message = "Your personal page.";

             return View();
         }*/

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        /*
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            var model = new CourseModel();
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Uploads"), fileName);
                    model.ServerPath = path;
                    file.SaveAs(path);
                    
                }
            }
        }*/
        [HttpGet]
        public PartialViewResult Timer()
        {
            return PartialView();
        }
    }
}