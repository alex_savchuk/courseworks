﻿using CourseBase.Entities;
using CourseBase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CourseBase.Controllers
{
    [Authorize (Roles="Operator")]
    public class WorkController: Controller
    {
        private readonly IWorkRepository _wr;

        public WorkController(IWorkRepository wr)
        {
            _wr = wr;
        }

        public ActionResult About()
        {
            return View(_wr.GetList(User.Identity.Name));
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new Work());
        }

        [HttpPost]
        public ActionResult Create(Work model)
        {
            if (ModelState.IsValid)
            {
                _wr.Create(model, User.Identity.Name);
                return RedirectToAction("About");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Work model)
        {
            if (ModelState.IsValid && _wr.HasAccess(model.IdW, User.Identity.Name))
            {
                _wr.Update(model);
                return RedirectToAction("About");
            }
            return View(model);
        }

        public ActionResult Remove(int id)
        {
            if(_wr.HasAccess(id, User.Identity.Name))
            {
                _wr.Remove(id);
            }

            return RedirectToAction("About");
        }
    }
}