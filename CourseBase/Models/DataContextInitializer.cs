﻿using CourseBase.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CourseBase.Models
{
    public class DataContextInitializer : DropCreateDatabaseAlways<CourseContext>
    {
        protected override void Seed(CourseContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Operator"));

            var userManager = new UserManager<User>(new UserStore<User>(context));
            userManager.Create(new User
            {
                Email = "oleksandr.savchuk@oa.edu.ua",
                UserName = "oleksandr.savchuk@oa.edu.ua"
            }, "Iburuf02!");

            var user = context.Users.First(x => x.Email == "oleksandr.savchuk@oa.edu.ua");
            userManager.AddToRole(user.Id, "Admin");
            userManager.AddToRole(user.Id, "Operator");

            context.SaveChanges();

            for (int j = 0; j < 4; ++j)
            {
                var @work = new Work
                {
                    OperatorW = user,
                    TitleW = "Work" + j
                };
                work = context.Works.Add(@work);
                context.SaveChanges();

                for (var i=0; i<10; ++i)
                {
                    var c = new Course
                    {
                        DateC = DateTime.Now.AddDays(i),
                        Work = @work,
                        IdC = Guid.NewGuid(),
                        PathC = "",
                        Number = i.ToString(),
                        TitleC = "Course #" + i
                    };
                    context.Courses.Add(c);
                }
                
            }
            context.SaveChanges();
            base.Seed(context);
        }
    }
}