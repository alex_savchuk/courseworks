﻿using CourseBase.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CourseBase.Models
{
    public class CourseContext : IdentityDbContext<User>
    {
        public CourseContext()
            : base("CourseContext", throwIfV1Schema: false)
        {

        }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Work> Works { get; set; }

        public static CourseContext Create()
        {
            return new CourseContext();
        }
    }
   
}