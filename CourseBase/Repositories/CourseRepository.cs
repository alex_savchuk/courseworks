﻿using CourseBase.Entities;
using CourseBase.Models;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CourseBase.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        public void Create(Course model, int wo, string path)
        {
            using (var ctx = new CourseContext())
            {
                model.IdC = Guid.NewGuid();
                model.PathC = path;
                //model.Work = ctx.Works.First(x => x.IdW = wo);
                ctx.Courses.Add(model);
                ctx.SaveChanges();
            }
        }

        public Course Get(string id)
        {
            using (var ctx = new CourseContext())
            {
                var guid = Guid.Parse(id);
                return ctx.Courses.Include(x => x.Work).First(x => x.IdC == guid);
            }
        }

        public List<Course> GetListByWork(int id)
        {
            using (var ctx = new CourseContext())
            {
                return ctx.Courses.Where(x => x.Work.IdW == id).ToList();
            }
        }

        public void Remove(string id, HttpServerUtilityBase server)
        {
            using (var ctx = new CourseContext())
            {
                var guid = Guid.Parse(id);
                var c = ctx.Courses.First(x => x.IdC == guid);

                if (!string.IsNullOrEmpty(c.PathC) && File.Exists(c.PathC))
                    File.Delete(c.PathC);

                ctx.Courses.Remove(c);
                ctx.SaveChanges();
            }
        }

        public Course TryGetCourse(string number)
        {
            using (var ctx = new CourseContext())
            {
                return ctx.Courses.Include(x => x.Work).FirstOrDefault(x => x.TitleC.ToLower() == number.ToLower());
            }
        }
    }
}