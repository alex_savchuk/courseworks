﻿using CourseBase.Entities;
using CourseBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace CourseBase.Repositories
{
    public class WorkRepository /// IWorkRepository
    {
        public void Create(Work model, string username)
        {
            using (var ctx = new CourseContext())
            {
                model.OperatorW = ctx.Users.First(x => x.UserName == username);
                ctx.Works.Add(model);
                ctx.SaveChanges();
            }
        }

        public Work Get(int id)
        {
            using (var ctx = new CourseContext())
            {
                return ctx.Works.First(x => x.IdW == id);
            }
        }

        public List<Work> GetList(string username)
        {
            using (var ctx = new CourseContext())
            {
                return ctx.Works.Include(x => x.Courses).Where(x => x.OperatorW.UserName == username).ToList();

            }
        }

        public bool HasAccess(int id, string username)
        {
            using (var ctx = new CourseContext())
            {
                return ctx.Users.Include(x => x.Works).Any(z => z.UserName == username && z.Works.Any(y => y.IdW == id));
            }
        }

        public void Remove(int id)
        {
            using (var ctx = new CourseContext())
            {
               // ctx.Courses.RemoveRange(ctx.Courses.Where(x => x.Work.Id == id));
                ctx.Works.Remove(ctx.Works.First(x => x.IdW == id));
                ctx.SaveChanges();
            }
        }

        public void Update(Work model)
        {
            using (var ctx = new CourseContext())
            {
                var w = ctx.Works.First(x => x.IdW == model.IdW);
                w.TitleW = model.TitleW;
                ctx.SaveChanges();
            }
        }
    }
    
}