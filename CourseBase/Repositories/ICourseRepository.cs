﻿using CourseBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseBase.Repositories
{
    public interface ICourseRepository
    {
        List<Course> GetListByWork(int id);
        void Create(Course model, int wo, string path);
        Course Get(string id);
        void Remove(string id, System.Web.HttpServerUtilityBase server);
        Course TryGetCourse(string number);
    }
}
