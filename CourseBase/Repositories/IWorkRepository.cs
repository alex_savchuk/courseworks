﻿using CourseBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseBase.Repositories
{
    public interface IWorkRepository
    {
        List<Work> GetList(string username);
        void Create(Work model, string username);
        bool HasAccess(int id, string username);
        void Remove(int Id);
        Work Get(int id);
        void Update(Work model);
    }
}
