﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CourseBase.Startup))]
namespace CourseBase
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
