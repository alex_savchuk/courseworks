﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CourseBase.Entities
{
    public class Course
    {
        [Key]
        public Guid IdC { get; set; }
        public string Number { get; set; }
        public string TitleC { get; set; }
        public DateTime? DateC { get; set; }
        public string PathC { get; set; }
        public Work Work { get; set; }
        public byte[] FileC { get; set; }
    }
}