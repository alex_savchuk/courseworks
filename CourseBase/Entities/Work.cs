﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CourseBase.Entities
{
    public class Work
    {
        [Key]
        public int IdW { get; set; }
        [Required]
        public string TitleW { get; set; }
        public User OperatorW { get; set; }
        public DateTime? DateW { get; set; }

        public ICollection<Course> Courses { get; set; }
    }
}