﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CourseBase.Entities
{
    public class CourseModel
    {
        [Key]
        public int Id { get; set; }
        public string ServerPath { get; set; }
    }
}